FROM quay.io/inotaykrisztian/openjdk:8u322

COPY target/*.jar /app.jar

EXPOSE 8080

ENTRYPOINT ["/bin/bash", "-c"]
CMD ["java \
 -XX:+UnlockExperimentalVMOptions \
 -Dserver.port=8080 \
 -jar app.jar \
"]
